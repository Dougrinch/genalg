package genalg

import java.util.Random

case class GenAlg[C: Chromosome, F: Ordering](firstPopulation: Population[C], fitness: Fitness[C, F]) {
    final val POPULATION_SIZE: Int = 1000
    final val ELITE_RATE: Double = 0.1
    final val SURVIVE_RATE: Double = 0.5
    final val MUTATION_RATE: Double = 0.2
    final val MAX_ITERATIONS: Int = 1000

    private implicit val chromosomeOrdering: Ordering[C] = Ordering.by(fitness.calc)
    private implicit val random = new Random()

    def go(): C = {
        println("Start")

        var population = firstPopulation.sorted

        println("First population generated")

        println(population.map(_.asInstanceOf[Tree].nodeCount()).sum.toDouble / population.size.toDouble)

        var i = 0
        while (i < MAX_ITERATIONS) {
            val f = fitness.calc(population(0))

            println(f)

            if (f == 0) {
                return population(0)
            }

            population = mate(population)

            i += 1
        }

        println("Not found")

        population(0)
    }

    private def mate(oldPopulation: Population[C]): Population[C] = {
        val population = oldPopulation.sorted

        val eliteSize = (POPULATION_SIZE * ELITE_RATE).asInstanceOf[Int]
        var children = selectElite(population, eliteSize)

        var i = eliteSize
        while (i < POPULATION_SIZE) {
            val parent1: Int = (Math.random * POPULATION_SIZE * SURVIVE_RATE).asInstanceOf[Int]
            val parent2: Int = (Math.random * POPULATION_SIZE * SURVIVE_RATE).asInstanceOf[Int]

            var child = implicitly[Chromosome[C]].cross(population(parent1), population(parent2))
            if (Math.random < MUTATION_RATE) {
                child = implicitly[Chromosome[C]].mutate(child)
            }

            children += child
            i += 1
        }

        children
    }

    private def selectElite(population: Population[C], eliteSize: Int): Population[C] = {
        var elite = Population[C](Nil)

        var i: Int = 0
        while (i < eliteSize) {
            elite += population(i)
            i += 1
        }

        elite
    }
}
