package genalg

import Tree.Num
import java.util.Random

sealed abstract class Tree {
    def eval(params: Map[String, Num]): Num
    def nodeCount(): Int
    def randomNode(implicit random: Random): Tree
    def replaceRandomNode(tree: Tree)(implicit random: Random): Tree
}

case class Const(value: Num) extends Tree {
    override def eval(params: Map[String, Num]): Num = value
    override def toString: String = String.valueOf(value)
    override def nodeCount(): Int = 1
    override def randomNode(implicit random: Random): Tree = this
    def replaceRandomNode(tree: Tree)(implicit random: Random): Tree = tree
}

case class Param(name: String) extends Tree {
    override def eval(params: Map[String, Num]): Num = params(name)
    override def toString: String = name
    override def nodeCount(): Int = 1
    override def randomNode(implicit random: Random): Tree = this
    def replaceRandomNode(tree: Tree)(implicit random: Random): Tree = tree
}

case class UnaryMinus(value: Tree) extends Tree {
    override def eval(params: Map[String, Num]): Num = -value.eval(params)
    override def toString: String = "-(" + value.toString + ")"
    override def nodeCount(): Int = value.nodeCount() + 1
    override def randomNode(implicit random: Random): Tree =
        if (random.nextInt(nodeCount()) == 0) this
        else value.randomNode
    def replaceRandomNode(tree: Tree)(implicit random: Random): Tree =
        if (random.nextInt(nodeCount()) == 0) tree
        else UnaryMinus(value.replaceRandomNode(tree))
}

case class Plus(left: Tree, right: Tree) extends Tree {
    override def eval(params: Map[String, Num]): Num = left.eval(params) + right.eval(params)
    override def toString: String = "(" + left.toString + " + " + right.toString + ")"
    override def nodeCount(): Int = left.nodeCount() + right.nodeCount() + 1
    override def randomNode(implicit random: Random): Tree = random.nextInt(nodeCount()) match {
        case 0 => this
        case n if n <= left.nodeCount() => left.randomNode
        case _ => right.randomNode
    }
    def replaceRandomNode(tree: Tree)(implicit random: Random): Tree = random.nextInt(nodeCount()) match {
        case 0 => tree
        case n if n <= left.nodeCount() => Plus(left.replaceRandomNode(tree), right)
        case _ => Plus(left, right.replaceRandomNode(tree))
    }
}

case class Minus(left: Tree, right: Tree) extends Tree {
    override def eval(params: Map[String, Num]): Num = left.eval(params) - right.eval(params)
    override def toString: String = "(" + left.toString + " - " + right.toString + ")"
    override def nodeCount(): Int = left.nodeCount() + right.nodeCount() + 1
    override def randomNode(implicit random: Random): Tree = random.nextInt(nodeCount()) match {
        case 0 => this
        case n if n <= left.nodeCount() => left.randomNode
        case _ => right.randomNode
    }
    def replaceRandomNode(tree: Tree)(implicit random: Random): Tree = random.nextInt(nodeCount()) match {
        case 0 => tree
        case n if n <= left.nodeCount() => Minus(left.replaceRandomNode(tree), right)
        case _ => Minus(left, right.replaceRandomNode(tree))
    }
}

case class Mul(left: Tree, right: Tree) extends Tree {
    override def eval(params: Map[String, Num]): Num = left.eval(params) * right.eval(params)
    override def toString: String = "(" + left.toString + " * " + right.toString + ")"
    override def nodeCount(): Int = left.nodeCount() + right.nodeCount() + 1
    override def randomNode(implicit random: Random): Tree = random.nextInt(nodeCount()) match {
        case 0 => this
        case n if n <= left.nodeCount() => left.randomNode
        case _ => right.randomNode
    }
    def replaceRandomNode(tree: Tree)(implicit random: Random): Tree = random.nextInt(nodeCount()) match {
        case 0 => tree
        case n if n <= left.nodeCount() => Mul(left.replaceRandomNode(tree), right)
        case _ => Mul(left, right.replaceRandomNode(tree))
    }
}

case class Div(left: Tree, right: Tree) extends Tree {
    override def eval(params: Map[String, Num]): Num = left.eval(params) / right.eval(params)
    override def toString: String = "(" + left.toString + " / " + right.toString + ")"
    override def nodeCount(): Int = left.nodeCount() + right.nodeCount() + 1
    override def randomNode(implicit random: Random): Tree = random.nextInt(nodeCount()) match {
        case 0 => this
        case n if n <= left.nodeCount() => left.randomNode
        case _ => right.randomNode
    }
    def replaceRandomNode(tree: Tree)(implicit random: Random): Tree = random.nextInt(nodeCount()) match {
        case 0 => tree
        case n if n <= left.nodeCount() => Div(left.replaceRandomNode(tree), right)
        case _ => Div(left, right.replaceRandomNode(tree))
    }
}

object Tree {
    type Num = Double

    object TreeOfXAsChromosome extends Chromosome[Tree] {
        override def generate(implicit random: Random): Tree = generate(0)

        private def generate(depth: Int)(implicit random: Random): Tree =
            if (depth >= 100) random.nextInt(2) match {
                case 0 => Const(random.nextInt(50))
                case 1 => Param("x")
            }
            else random.nextInt(5) match {
                case 0 | 1 => random.nextInt(2) match {
                    case 0 => Const(random.nextInt(50))
                    case 1 => Param("x")
                }
                case 2 => random.nextInt(1) match {
                    case 0 => UnaryMinus(generate(depth + 1))
                }
                case 3 | 4 => random.nextInt(4) match {
                    case 0 => Plus(generate(depth + 1), generate(depth + 1))
                    case 1 => Minus(generate(depth + 1), generate(depth + 1))
                    case 2 => Mul(generate(depth + 1), generate(depth + 1))
                    case 3 => Div(generate(depth + 1), generate(depth + 1))
                }
            }

        override def cross(first: Tree, second: Tree)(implicit random: Random): Tree =
            first.replaceRandomNode(second.randomNode)

        override def mutate(gen: Tree)(implicit random: Random): Tree =
            gen.replaceRandomNode(generate)
    }
}