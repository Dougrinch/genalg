package genalg

import genalg.Tree.Num
import java.util.Random

object Main extends App {

    implicit val chromosome = Tree.TreeOfXAsChromosome
    implicit val random = new Random()

    val x = ((-100 to 100) map (n => (n, n / (145 + n)))).toMap

    val genalg = new GenAlg[Tree, Double](
        Population((1 to 1000).map(_ => chromosome.generate).toList),
        new Fitness[Tree, Double] {
            override def calc(genome: Tree): Double = {
                val res = (x map (p => calc(genome, p._1, p._2))).sum
                if (res < 0) Long.MaxValue
                else res
            }

            private def calc(genome: Tree, x: Num, y: Num): Long = {
                val res = genome.eval(Map("x" -> x))
                if (res == Double.NegativeInfinity) Long.MaxValue
                else if (res == Double.NaN) Long.MaxValue
                else Math.abs(y.toLong - res.toLong)
            }
        })

    val result = genalg.go()
    println(result)
    println(genalg.fitness.calc(result))
}
