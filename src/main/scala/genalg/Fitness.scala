package genalg

abstract class Fitness[C: Chromosome, F: Ordering] {

    def calc(genome: C): F
}
