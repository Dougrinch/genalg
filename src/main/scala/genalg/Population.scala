package genalg

import java.util.Random

case class Population[C: Chromosome](chromosomes: List[C]) extends Iterable[C] {

    def apply(index: Int): C = chromosomes(index)

    def +(chromosome: C): Population[C] = addChromosome(chromosome)

    def addChromosome(chromosome: C): Population[C] = Population(chromosome :: chromosomes)

    def randomChromosome(implicit random: Random): C = chromosomes(random.nextInt(chromosomes.size))

    def sorted(implicit ord: Ordering[C]): Population[C] = Population(chromosomes.sorted)

    override def iterator: Iterator[C] = chromosomes.iterator
}
