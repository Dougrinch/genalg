package genalg

import java.util.Random

abstract class Chromosome[C] {

    def generate(implicit random: Random): C
    def cross(first: C, second: C)(implicit random: Random): C
    def mutate(gen: C)(implicit random: Random): C
}
