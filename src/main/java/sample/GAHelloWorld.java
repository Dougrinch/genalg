package sample;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GAHelloWorld {

    public static final int POPULATION_SIZE = 1000;
    public static final double ELITE_RATE = 0.1;
    public static final double SURVIVE_RATE = 0.5;
    public static final double MUTATION_RATE = 0.2;
    public static final String TARGET = "Hello World!";
    private static final int MAX_ITER = 1000;

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            System.out.println(new GAHelloWorld().go());
        }
    }

    private int go() {
        List<Genome> population = initializePopulation();

        for (int i = 0; i < MAX_ITER; i++) {
            Collections.sort(population);
            //System.out.println(i + " > " + population.get(0));

            if (population.get(0).fitness == 0) {
                return i;
            }

            population = mate(population);
        }

        return -1;
    }

    private List<Genome> initializePopulation() {
        List<Genome> population = new ArrayList<Genome>();
        int targetSize = TARGET.length();

        for (int i = 0; i < POPULATION_SIZE; i++) {
            StringBuilder str = new StringBuilder();
            for (int j = 0; j < targetSize; j++) {
                str.append((char) (Math.random() * 255));
            }
            Genome citizen = new Genome(str.toString());
            population.add(citizen);
        }
        return population;
    }

    private List<Genome> mate(List<Genome> population) {
        int eliteSize = (int) (POPULATION_SIZE * ELITE_RATE);
        int targetSize = TARGET.length();

        List<Genome> children = new ArrayList<Genome>();

        selectElite(population, children, eliteSize);

        for (int i = eliteSize; i < POPULATION_SIZE; i++) {
            int parent1 = (int) (Math.random() * POPULATION_SIZE * SURVIVE_RATE);
            int parent2 = (int) (Math.random() * POPULATION_SIZE * SURVIVE_RATE);
            int splitPos = (int) (Math.random() * targetSize);

            String str = population.get(parent1).str.substring(0, splitPos) +
                    population.get(parent2).str.substring(splitPos);
            if (Math.random() < MUTATION_RATE) {
                str = mutate(str);
            }
            Genome child = new Genome(str);
            children.add(child);
        }
        return children;
    }

    private void selectElite(List<Genome> population, List<Genome> children, int eliteSize) {
        for (int i = 0; i < eliteSize; i++) {
            children.add(population.get(i));
        }
    }

    private String mutate(String str) {
        int targetSize = TARGET.length();
        int charToMutatePos = (int) (Math.random() * targetSize);
        char delta = (char) (Math.random() * 255);

        return str.substring(0, charToMutatePos) + delta + str.substring(charToMutatePos + 1);
    }
}

class Genome implements Comparable<Genome> {

    final String str;
    final int fitness;

    public Genome(String str) {
        this.str = str;
        int fitness = 0;
        for (int j = 0; j < str.length(); j++) {
            fitness += Math.abs(str.charAt(j) - GAHelloWorld.TARGET.charAt(j));
        }
        this.fitness = fitness;
    }

    @Override
    public int compareTo(Genome o) {
        return fitness - o.fitness;
    }

    @Override
    public String toString() {
        return fitness + " " + str;
    }
}
