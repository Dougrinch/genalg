package genalg

import org.testng.annotations.Test
import java.util.concurrent.ThreadLocalRandom
import genalg.Tree.TreeOfXAsChromosome

class TreeTest {

    @Test
    def testEval() {
        1 to 10 foreach (n => {
            val tree = TreeOfXAsChromosome.generate(ThreadLocalRandom.current())
            println(s"${tree.eval(Map("x" -> 0))}: $tree")
        })
    }
}
